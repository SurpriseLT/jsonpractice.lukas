﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace JsonPractice
{
    public partial class Form1 : Form
    {
        public Asmuo Asmuo { get; set; }
        public Form1()
        {
            InitializeComponent();
            Asmuo = new Asmuo
            {
                Vardas = "Jonas",
                Pavarde = "Jonaitis",
                TelNr = "+3705456358",
                Adresas = new _Adresas
                {
                    Salis = "Lietuva",
                    Miestas = "Siauliai",
                    Gatve = "Laisves"
                }
            };

            var asmuoJson = JsonConvert.SerializeObject(Asmuo);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
    public class Asmuo
    {
        public string Vardas { get; set; }
        public string Pavarde { get; set; }
        public string TelNr { get; set; }
        public _Adresas Adresas { get; set; }
    }
    public class _Adresas
    {
        public string Salis { get; set; }
        public string Miestas { get; set; }
        public string Gatve { get; set; }
    }
}
